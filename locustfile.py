from locust import FastHttpUser, between, task
import pandas as pd
import random

class MyWebsiteUser(FastHttpUser):
    """
    This is a class of simulated users that gets run when the test is running.
    It extends FastHttpUser. Also, it has wait_time property.
    """
    # wait_time is used to specify how long a simulated user should wait
    # between executing tasks
    wait_time = between(0, 0)

    # Decorator task is used to specify that the load_main method is a task
    # that should get executed by the simulated user.
    @task
    def load_main(self):
        """
        Randomly select an url. Then query fastapi server for node's address.
        """
        # randomly select a file containing url
        file_name = "/home/test_data_t" + str(random.randint(1, 100)) + ".csv"
        # randomly select an url from a file
        df = pd.read_csv(file_name, header=None)
        url_hash = df[0].sample().values[0]
        # query fastapi server
        self.client.get("/get_node/" + url_hash)