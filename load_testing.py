import random
import time
from multiprocessing import Process, Manager

import pandas as pd
import numpy as np
from sqlalchemy import create_engine


def create_conn(conn_string):
    return create_engine('postgresql+psycopg2://' + conn_string).connect()

def execute_query(conn_string, query):
    _conn = create_conn(conn_string)
    rs = _conn.execute(query)
    return rs

def task(returned_dict, process_index):
    """
    Simulate a task to get a node's address from a url. 

    Parameters
    ----------
    returned_dict : dict
        A dict to save the response time.
    process_index : int
        the index of the task.
    """

    # randomly select a file containing url
    file_name = "/home/test_data_t" + str(random.randint(1, 100)) + ".csv"
    # randomly select a url from a file
    df = pd.read_csv(file_name, header=None)
    url_hash = df[0].sample().values[0]

    # query to get node from url
    conn_string = 'postgres:@/postgres?host=/var/run/postgresql'
    query = "select distinct hexa_number from sharding where url_hash = decode('" + url_hash + "', 'hex')"

    start_time = time.time()
    execute_query(conn_string, query)
    response_time = int((time.time() - start_time) * 1000)
    returned_dict[process_index] = response_time 


def experiment(rps, number_repeat):
    """
    Run load testing with `rps` requests per second. Then, compute the database's
    response time. We repeat this in `number_repeat` times and compute the average. 

    Parameters
    ----------
    rps : float
        the requests per second to the database.
    number_repeat : int
        the number of times to repeat
    """

    manager = Manager()
    returned_dict = manager.dict()
    process_list = []

    c = 0
    while c < number_repeat:
        i = 0
        end_time = time.time() + 1
        while time.time() < end_time:
            if i >= rps:
                break
            process = Process(
                target=task,
                kwargs={
                    'returned_dict': returned_dict,
                    'process_index': c*rps + i
                })

            process.start()
            process_list.append(process)
            i += 1
        c += 1
 
    for process in process_list:
        process.join()

    return (
        len(returned_dict) * 1.0/number_repeat,
        np.average(returned_dict.values())
        )

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=("Run load testing. By increase the number of request per" +
            "second until the peak `max_rps` by `increased_rate`. Repeat the " + 
            "task to compute the average."))
    parser.add_argument("--max_rps", type=int,
                        help="Peak number of request per second, in times/sec")
    parser.add_argument("--increased_rate", type=int,
                        help="the number of requests per second increases, in times/sec")
    parser.add_argument("--number_repeat", type=int,
                        help="the number to repeat the same experimet, in times")

    args = parser.parse_args()
    max_rps = args.max_rps
    increased_rate = args.increased_rate
    number_repeat = args.number_repeat

    result = []
    # increase the request per second until max_rps by increased_rate
    for rps in range(1, max_rps+1, increased_rate):
        executed_rps, res_time = experiment(rps, number_repeat)
        print(rps, executed_rps, res_time)
        result.append([rps, executed_rps, res_time])

    pd.DataFrame(
        result, 
        columns=['rps', 'executed_rps', 'res_time']
        ).to_csv(
            f'log/self_made_script_{max_rps}_{increased_rate}_{number_repeat}.csv'
            )