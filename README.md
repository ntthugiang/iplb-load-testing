# IPLB Load Testing

## Test Task Architecture
![Test task Architecture](fig/GRAPH002.jpg)

## Requirements
These are the necessary packages to run IPLB load testing.
* locust==2.8.3
* sqlalchemy==1.4.31
* psycopg2==2.9.3
* fastapi==0.75.0
* uvicorn==0.17.5
* databases==0.5.5
* asyncpg==0.25.0
* pandas==1.4.1
* jupyter==1.0.0
* matplotlib==3.5.1

## Environment
This is the specs of the test server.
* OS: Ubuntu 21.10
* CPU: Intel Core Processor (Haswell, no TSX), 2 CPUs, 2GHz
* Memory: 8G
  
## Installation
Firstly, we clone this repository.
```bash
git clone https://gitlab.com/ntthugiang/iplb-load-testing.git
```
Then, we create a virtual environment to install required packages.
```bash
sudo apt install -y python3-virtualenv
cd ~/iplb-load-testing
virtualenv --python=/usr/bin/python3 venv
source venv/bin/activate
pip install -r requirements
```

## Running
I have two methods to do load testing. In both methods, I increased the number of requests per second from 1 to 100 one by one. In the results, I recorded the actual number of requests per second executed and the average response time. 

1. Use `Locust` framework to do load testing.
   
    First, start FastAPI server (http://127.0.0.1:8000) to simulate the request to PostgreSQL
    ```bash
    uvicorn server:app
    ```
    Next, Run `Locust` without GUI to do the load testing. 
    ```bash
    locust -f locustfile.py -H http://127.0.0.1:8000 --headless -u 100 -r 1 -t 3m30s --csv log/locust_100_1  --html log/locust_100_1.html
    ```
    - `-u` is the peak number of concurrent users to make the request. I set it to 100. It is similar to the max number (100) of requests per second.
    - `-r` is the rate to spawn users at (users per second). I set it to 1. It is similar to increasing the number of request per second one by one.
    - `-t` is the amount of time to run the test. I set it to 3m30s. It is the approximate time to reach the peak number of concurrent users in this load test machine.
    - `--csv` to record the results to csv files. I set it to log/locust_100_1
    - `--html` to record the results to html file. I set it to log/locust_100_1.html

2. Use self-made load testing script.

    This script will record the results into `log/self_made_script_100_1_10.csv`.
    ```
    python load_testing.py --max_rps 100 --increased_rate 1 --number_repeat 10
    ```
    - `--max_rps` is the max number of requests per second. I set it to 100.
    - `--increased_rate` is the rate to increase the number of requests per second. I set it to 1. It means increasing one by one.
    - `--number_repeat` is the times to repeat the experiment to estimate the average of the actual number of requests per second and the response time. I set it to 10.

## Result & Discussion
I used Jupyter Notebook (`plot_graph.ipynb`) to generate the plots from `log/self_made_script_100_1_10.csv` and `log/locust_100_1_stats_history.csv`.

I wanted to find the limit with PostgreSQL. Because the resources prepared for me is limited, I couldn't generate enough the number of requests per second to query the database.

The below graph shows that in both `Locust` and self-made script, although I increased the number of request per second, I generated up to approximate 20 requests per second.

![Executed RPS](fig/executed_rps.png "Executed Requests Per Second")

The below graph shows the average reponse time when I increased the number of request per second. 

![Average Response Time](fig/average_response_time.png "Average Response Time")

In the self-made script's graph, after the number of executed requests per second reaching to the peak (approximate 20), the average response time didn't change almostly. Probably, PostgreSQL worked well with approximate 20 requests per second. And in my program, I could not increase the number of requests (to 100 or above) just in one second.

In the `Locust`'s graph, the average reponse time increased when I increased the number of requests per second. I don't know why this happened because I don't have enough time to learn more about `Locust`. Probably, because I queried PostgreSQL via FastAPI, so there are some loads related to it.

In this loading test, I could not find the limit of PostgreSQL but I found the limit of the number of requests per second to PostgreSQL on this test server. 

## About Architecture

![Architecture](fig/GRAPH001.jpg)

1. From my load testing, we know that the Python script can take a lot of resources, then it can affect PostgreSQL's performance. We should seperate PostgreSQL DB from Python Script (HTTP Server).
2. From my load testing, we know that to run Python Script in one machine is not enough for million requests per minute. We should use distributed HTTP Server. 
3. We know that PostgreSQL has a limit on the number of connection in the same time. In the high traffic, we should buffer users' calling API in the HTTP server to query PostgreSQL DB in just one time.
4. We can use caches in HTTP server, so we are not necessary to query PostgreSQL every time.