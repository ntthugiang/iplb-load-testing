from fastapi import FastAPI
import databases

DATABASE_URL = "postgresql+psycopg2://postgres:@/postgres?host=/var/run/postgresql"
database = databases.Database(DATABASE_URL)

app = FastAPI()

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

@app.get("/get_node/{url_hash}")
async def get_node(url_hash):
    """
    Get node's address from an url.
    """
    query = "select distinct hexa_number from sharding where url_hash = decode('" + url_hash + "', 'hex')"

    rs = await database.fetch_all(query)

    return {"hexa_number": rs[0].hexa_number}